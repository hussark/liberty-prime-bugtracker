# Welcome

To report any issue, just go to [+ New Issue](https://bitbucket.org/hussark/liberty-prime-bugtracker/issues/new) and explain it carefully, screenshots are welcome, but details is what makes issue tracking more easy to us.